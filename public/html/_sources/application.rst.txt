
About the OpenSCAD Application
===============================

The application has four main windows:
    * Viewing Area
    * Editor
    * Console
    * Customizer

The 'Window' menu can show or hide these windows. 

After entering/changing the code in the editor, press f5 to see a preview
The console window may indicate errors.

Press F6 to see the final rendering

Press F7 to export the model as STL(*) file 
    
The built-in editor can be replaced by an external editor. Under the menu 'Design' there is an option: 'Automatic reload'. OpenSCAD creates an new preview, whenever the file is changed.

The online documentation can be reached with a link in the help menu.

(*) can be configured

