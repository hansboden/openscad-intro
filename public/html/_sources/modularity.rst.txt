Modularity
==========

As projects grow bigger, models are composed of smaller parts and there can be many of them. To handle a large number of OpenSCAD objects, there are a number of techniques:

* the 'customizer' for alternative generations
* the 'include' instruction
* the 'use' instruction

The Customizer
--------------
In the OScad menu, its possible to show or hide different windows. The customizer window allows to temporarily change the value of variables in the global scope. In the following example, we use this feature to generate different objects from the same source code::

    // Sample code for the Customizer feature
    // of OpenSCAD

    model_select = "base";  // visible to the customzer
    custom(model_select);

    module custom(m) {  / create a local scope
        box_dim = [50, 40, 20];  // hidden from the customizer
        tap_height = 8;

        if (m == "base") { base(); } 
        if (m == "tap") { tap(); }
            
        module base() {
            b = box_dim;
            difference() {
                cube(b);
                translate([2,2,2])
                    cube([b.x-4, b.y-4, b.z]);
            }
        }

        module tap() {
            b = box_dim;
            difference() {
                cube([b.x+4, b.y+4, tap_height]);
                translate([2,2,2])
                    cube([b.x, b.y, tap_height]);
            }
        }
    }


By changing 'model_select' the same code renders different objects. 

.. image:: img/customizer_1.png    

.. image:: img/customizer_2.png    

In the above example the module 'custom' was used only to hide the 'box_dim' and 'tap_height' from the customizer, which only sees global names.


The include statement
---------------------
For larger projects it may be convenient to split the code into separate source files. However some values (like sizes and distances) or module must be shared. The above example could be changed to show the use of separate source files::

    // Sample code for the include statement.
    // this is the box, which uses globally defines dimensions

    include <incl_dims.scad>

    box();

    module box() {
        b = box_dim;  // the name is defined in the included file
        difference() {
            cube(b);
            translate([2,2,2])
                cube([b.x-4, b.y-4, b.z]);
        }
    }
    
    
    // Sample code for the include statement.
    // this is the tap, which uses globally defines dimensions

    include <incl_dims.scad>

    tap();

    module tap() {
        b = box_dim;
        difference() {
            cube([b.x+4, b.y+4, tap_height]);
            translate([2,2,2])
                cube([b.x, b.y, tap_height]);
        }
    }
    
    
The filename path in the include statement may be absolute or relative, here it just refers to the same directory as the calling source code::

    // Sample code for the include statement.
    // this is the included data (just dimension values)

    box_dim = [50, 40, 20];
    tap_height = 8;

The include statement essentially inserts the included file into the source code. Everything in the included file is treated as defined locally.


The 'use' statement
-------------------
The use statement works similar to the include. However the text is not inserted, but executed, and the resulting module and function definitions are made available locally. The use statement hides the variarables in the imported text.

The use statement is more convenient for modules, which are generally useful for different projects. The example for the bevel_cube shows, that the color definition in the local file does not affect the imported module::

    // 'use' this file which is stored as
    //      ./oscadlib/basic.scad
    $fn = 4;
    libcor = "cyan";

    module bevel_cube(dim, rad=2, $fn=16) {
        color(libcor) 
        hull() {
            for (x=[+rad, dim.x-rad], y=[+rad, dim.y-rad], z=[+rad, dim.z-rad]) {
                translate([x,y,z]) sphere(r=rad);
    }   }   }

    
    // test 'use' files
    $fn = 64;
    libcor="silver";

    use <../oscadlib/basic.scad>

    bevel_cube([30,50,15],4);
    translate([-30,20,20]) sphere(15);

    
.. image:: img/use_statement.png    
    
